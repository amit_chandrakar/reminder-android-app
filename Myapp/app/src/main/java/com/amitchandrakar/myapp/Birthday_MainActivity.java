package com.amitchandrakar.myapp;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Birthday_MainActivity extends Activity
{
    ListView DOB_ListView;
    ProgressBar progressBar;
    String HttpURL = "http://maierp.in/android_live/demo/fetch_user_list.php";
    BirthdayListAdapter adapter ;
    List<DOB> DOB_List;
    EditText editText ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.birthday_activity_main);
        DOB_ListView = (ListView) findViewById(R.id.DOB_ListView);
        editText = (EditText)findViewById(R.id.edittext1);
        progressBar = (ProgressBar) findViewById(R.id.ProgressBar1);
        new ParseJSonDataClass(this).execute();
    }

    private class ParseJSonDataClass extends AsyncTask<Void, Void, Void>
    {
        public Context context;
        String FinalJSonResult;
        public ParseJSonDataClass(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0)
        {
            Birthday_HttpServiceClass birthday_httpServiceClass = new Birthday_HttpServiceClass(HttpURL);
            try
            {
                birthday_httpServiceClass.ExecutePostRequest();
                if (birthday_httpServiceClass.getResponseCode() == 200)
                {
                    FinalJSonResult = birthday_httpServiceClass.getResponse();
                    if (FinalJSonResult != null)
                    {
                        JSONArray jsonArray = null;
                        try
                        {
                            jsonArray = new JSONArray(FinalJSonResult);
                            JSONObject jsonObject;
                            DOB dob;
                            DOB_List = new ArrayList<DOB>();
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                dob = new DOB();
                                jsonObject = jsonArray.getJSONObject(i);
                                dob.User_Name_Col = jsonObject.getString("uname");
                                dob.DOB_Col = jsonObject.getString("udob");
                                DOB_List.add(dob);
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                else
                {
                    Toast.makeText(context, birthday_httpServiceClass.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            progressBar.setVisibility(View.GONE);
            DOB_ListView.setVisibility(View.VISIBLE);
            adapter = new BirthdayListAdapter(DOB_List, context);
            DOB_ListView.setAdapter(adapter);
        }
    }
} // end of main class