package com.amitchandrakar.myapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;


public class Menu1 extends Fragment implements View.OnClickListener
{

    TextView already_user;
    EditText fullname, username, password, con_password, email, mobile;
    String Fullname, Username, Password, Con_password, Email, Mobile, Dob, Ann_date, Gender, Marital_Status;
    String ID = null;
    CheckBox terms_conditions;
    Button signup;
    ImageView cal_ann_date, cal_dob;
    EditText ET_an_date, ET_dob;
    private int mYear, mMonth, mDay;
    private int nYear, nMonth, nDay;

    String gender, status = "";

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_menu_1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Home Panel");

        fullname             = (EditText)getActivity().findViewById(R.id.fullname);
        username             = (EditText)getActivity().findViewById(R.id.username);
        password             = (EditText)getActivity().findViewById(R.id.password);
        con_password         = (EditText)getActivity().findViewById(R.id.con_password);
        email                = (EditText)getActivity().findViewById(R.id.email);
        mobile               = (EditText)getActivity().findViewById(R.id.mobile);
        already_user         = (TextView)getActivity().findViewById(R.id.already_user);

        cal_ann_date = (ImageView)getActivity().findViewById(R.id.btn_ann_date);
        ET_an_date=(EditText)getActivity().findViewById(R.id.ann_date);

        signup = (Button)getActivity().findViewById(R.id.signUpBtn);
        signup.setOnClickListener(this);

        cal_ann_date.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        ET_an_date.setText(dayOfMonth+ "-" + (monthOfYear + 1) + "-" +  year);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        cal_dob = (ImageView)getActivity().findViewById(R.id.btn_dob);
        ET_dob=(EditText)getActivity().findViewById(R.id.dob);
        cal_dob.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                nYear = c.get(Calendar.YEAR);
                nMonth = c.get(Calendar.MONTH);
                nDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        ET_dob.setText( year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, nYear, nMonth, nDay);
                datePickerDialog.show();
            }
        });

        RadioGroup get_gender = (RadioGroup) getActivity().findViewById(R.id.gender);
        get_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.male:
                        gender = "Male";
                        break;
                    case R.id.female:
                        gender = "Female";
                        break;
                }
            }
        });

        RadioGroup get_marital_status = (RadioGroup) getActivity().findViewById(R.id.marital_status);
        get_marital_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.married:
                        status = "Married";
                        ET_an_date.setVisibility(View.VISIBLE);
                        cal_ann_date.setVisibility(View.VISIBLE);
                        break;
                    case R.id.unmarried:
                        status = "Unmarried";
                        ET_an_date.setVisibility(View.INVISIBLE);
                        cal_ann_date.setVisibility(View.INVISIBLE);
                        ET_dob.setText("");
                        break;
                }
            }
        });


    }//onview created

    @Override
    public void onClick(View view) {
        int viedId = view.getId();
        switch (viedId)
        {
            case R.id.signUpBtn:
                Fullname            = fullname.getText().toString();
                Username            = username.getText().toString();
                Password            = password.getText().toString();
                Con_password        = con_password.getText().toString();
                Email               = email.getText().toString();
                Mobile              = mobile.getText().toString();
                Dob                 = ET_dob.getText().toString();
                Ann_date            = ET_an_date.getText().toString();
                Gender              = gender;
                Marital_Status      = status;

                String username_exp = "^[A-Za-z0-9]{4,15}$";
                String email_exp = "^[A-Za-z][A-Za-z0-9]*([._-]?[A-Za-z0-9]+)@[A-Za-z].[A-Za-z]{0,10}?.[A-Za-z]{0,10}$";

                if (Fullname.equals("") || Fullname.length() == 0)
                {
                    fullname.setError("This field cannot be empty");
                }
                else if (Username.equals("") || Username.length() == 0)
                {
                    username.setError("This field cannot be empty");
                }
                else if (!Username.matches(username_exp))
                {
                    username.setError("Contain A-Za-z0-9");
                }
                else if (Password.equals("") || Password.length() == 0)
                {
                    password.setError("This field cannot be empty");
                }
                else if ( Password.length() < 5)
                {
                    password.setError("Poor Password");
                }
                else if (!Password.matches(Con_password))
                {
                    con_password.setError("Confirm password not match");
                }
                else if (Email.equals("") || Email.length() == 0)
                {
                    email.setError("This field cannot be empty");
                }
                else if (!Email.matches(email_exp))
                {
                    email.setError("Email Incorrect");
                }
                else if (Mobile.equals("") || Mobile.length() == 0)
                {
                    mobile.setError("This field cannot be empty");
                }
                else if (Mobile.length()<10 || Mobile.length()>10)
                {
                    mobile.setError("Incorrect Mobile Number");
                }
                else
                {
                    BackGround b = new BackGround();
                    b.execute(Fullname, Username, Password, Email, Mobile,Dob, Ann_date, Gender, Marital_Status);
                }
            break;
        }
    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String FullName        = params[0];
            String User            = params[1];
            String Pass            = params[2];
            String Email           = params[3];
            String Mobile          = params[4];
            String Dob             = params[5];
            String Ann_date        = params[6];
            String Gender          = params[7];
            String M_status        = params[8];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/register.php");

                String urlParams = "full_name="+FullName
                        +"&username="+User
                        +"&password="+Pass
                        +"&email="+ Email
                        +"&mobile="+Mobile
                        +"&dob="+Dob
                        +"&ann_date="+ Ann_date
                        +"&gender="+Gender
                        +"&status="+M_status;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();
                return data;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s)
        {

            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                ID = user_data.getString("id");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            Intent i = new Intent(getActivity(), Varify_OTP.class);
            i.putExtra("id", ID);
            i.putExtra("err", err);
            startActivity(i);

        }
    }//end of background

    @Override
    public void onResume()
    {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK)
                {
//                    Toast.makeText(getActivity(), "Back press", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
    }

}//end of main class
