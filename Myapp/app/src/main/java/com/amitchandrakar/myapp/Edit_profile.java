package com.amitchandrakar.myapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Edit_profile extends AppCompatActivity
{
    String ID= null, NAME=null, PASSWORD=null, EMAIL=null, MOBILE=null, FULLNAME=null, msg = null;
    TextView nameTV, emailTV, passwordTV, mobileTV, fullnameTV;
    String uid;
    Context ctx=this;
    EditText et1,et2,et3,et4,et5;
    String GET_USERNAME, GET_FULLNAME, GET_EMAIL, GET_PASSWORD, GET_MOBILE;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        et1 = (EditText) findViewById(R.id.username);
        et2 = (EditText) findViewById(R.id.fullname);
        et3 = (EditText) findViewById(R.id.email);
        et4 = (EditText) findViewById(R.id.mobile);
        et5 = (EditText) findViewById(R.id.password);


        //sharedpreferences logic
        SharedPref sharedPref = new SharedPref(Edit_profile.this);
        uid = sharedPref.getId().toString();
        BackGround b = new BackGround();
        b.execute(uid);
        // sharedpreferences logic

        Button cancel = (Button)findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(Edit_profile.this, Profile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String id = params[0];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/profile.php");
                String urlParams = "id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }//end of do in background

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                msg = user_data.getString("msg");
                NAME = user_data.getString("name");
                PASSWORD = user_data.getString("password");
                EMAIL = user_data.getString("email");
                MOBILE = user_data.getString("mobile");
                FULLNAME = user_data.getString("fullname");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
                et1.setText(NAME);
                et2.setText(FULLNAME);
                et3.setText(EMAIL);
                et4.setText(PASSWORD);
                et5.setText(MOBILE);

            }
            else
            {
                Toast.makeText(ctx, "Info loading fails", Toast.LENGTH_LONG).show();
            }
        }//end of onpostexecute
    }//end of background

    public void On_Edit_Profile(View v)
    {
        GET_USERNAME            = et1.getText().toString();
        GET_FULLNAME            = et2.getText().toString();
        GET_EMAIL               = et3.getText().toString();
        GET_PASSWORD            = et4.getText().toString();
        GET_MOBILE              = et5.getText().toString();

        //sharedpreferences logic
        SharedPref sharedPref = new SharedPref(Edit_profile.this);
        uid = sharedPref.getId().toString();
        // sharedpreferences logic

        BackGround_Profile_Edit b = new BackGround_Profile_Edit();
        b.execute(uid, GET_USERNAME, GET_FULLNAME, GET_EMAIL, GET_PASSWORD, GET_MOBILE);

    }//end of on_register

    class BackGround_Profile_Edit extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String id              = params[0];
            String user_name            = params[1];
            String full_name            = params[2];
            String email           = params[3];
            String password          = params[4];
            String mobile          = params[5];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/edit_profile.php");
                String urlParams = "id="+id
                        +"&user_name="+user_name
                        +"&full_name="+full_name
                        +"&email="+email
                        +"&password="+ password
                        +"&mobile="+mobile;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }//end of do in background

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                msg = user_data.getString("msg");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
                Toast.makeText(ctx, "Success..! Profile Updated.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Profile.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(ctx, "Info loading fails", Toast.LENGTH_LONG).show();
            }
        }//end of onpostexecute
    }//end of background
}
