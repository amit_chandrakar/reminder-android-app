package com.amitchandrakar.myapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class Event_menu extends Fragment implements View.OnClickListener
{
    CardView add_event, ass_event_cal;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.event_menu_fragment, container, false);
    } 

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Add Event");
        add_event = (CardView)getActivity().findViewById(R.id.my_event);
        ass_event_cal = (CardView)getActivity().findViewById(R.id.ass_event);
        add_event.setOnClickListener(this);
        ass_event_cal.setOnClickListener(this);

    }//onview created

    @Override
    public void onClick(View view)
    {
        int viedId = view.getId();
        switch (viedId)
        {
            case R.id.my_event:
                Fragment fragment = new Add_event();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                Toast.makeText(getActivity(),"Add Event",Toast.LENGTH_SHORT).show();
                break;

            case R.id.ass_event:
//                Intent in = new Intent(getActivity(), Anniversary_MainActivity.class);
//                startActivity(in);
                Toast.makeText(getActivity(),"Event Calendar",Toast.LENGTH_SHORT).show();
                break;
        }
    }

}//end of main class


