package com.amitchandrakar.myapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Forgot_Password extends Activity
{

    View view;
    EditText emailmobile;
    String EmailMobile;
    TextView submit, back;
    Context ctx=this;
    String ID, MSG = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot__password);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();

        emailmobile = (EditText)findViewById(R.id.emailphone);
        submit = (TextView)findViewById(R.id.forgot_button);
        back = (TextView)findViewById(R.id.backToLoginBtn);

        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(Forgot_Password.this, Login.class);
                startActivity(intent);
            }
        });
    }

    public void OnForgotPassword(View v)
    {

        EmailMobile = emailmobile.getText().toString();
        //Toast.makeText(ctx, EmailMobile, Toast.LENGTH_LONG).show();
        if (EmailMobile.equals("") || EmailMobile.length() == 0)
        {
            emailmobile.setError("This field cannot be empty");
        }
        else
        {
            BackGround b = new BackGround();
            b.execute(EmailMobile);
        }

    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String emailmobile = params[0];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/forgot_password.php");
                String urlParams = "emailmobile="+emailmobile;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                //Toast.makeText(ctx, "Sorry..!", Toast.LENGTH_LONG).show();

                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                MSG = user_data.getString("msg");


                if (MSG.equals("SUCCESS"))
                {
                    ID = user_data.getString("id");
                    //Toast.makeText(ctx, "Sorry..!", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ctx, Reset_OTP.class);
                    i.putExtra("id", ID);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(ctx, "Account not found...!", Toast.LENGTH_LONG).show();
                }


            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }
        }
    }
}//end of main class
