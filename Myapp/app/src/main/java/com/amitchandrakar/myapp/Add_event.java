package com.amitchandrakar.myapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import static com.amitchandrakar.myapp.R.layout.add_event_fragment;
import static com.amitchandrakar.myapp.R.layout.event_menu_fragment;

public class Add_event extends Fragment implements Spinner.OnItemSelectedListener
{
    private Spinner spinner;
    private ArrayList<String> students;
    private JSONArray result;
    String uid;
    String ID= null, UID = null ,NAME=null, PASSWORD=null, EMAIL=null, MOBILE=null, FULLNAME=null, msg = null;
    EditText name,phone;

    RadioGroup rem_req;
    Spinner scrtocontactname, getCallByRoleID , user_list;
    EditText getComment;
    EditText getsummary;
    EditText getReminderFrom;
    EditText getReminderTo;
    Button backtoeventlist,addreminder;
    String Reminder_req,Source_to_contact , Comment, Summery, Rem_start, Rem_end, Dept, GetName, Caller_Name;


    //for date
    EditText from_date, to_date;
    private int mYear, mMonth, mDay;
    private int nYear, nMonth, nDay;
    String r;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(add_event_fragment, container, false);
    }// end of Add event class

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Add Event");

        students = new ArrayList<String>();
        spinner = (Spinner) view.findViewById(R.id.user_list);
        name = (EditText) getActivity().findViewById(R.id.getCallerName);
        name.setEnabled(false);
        phone = (EditText) getActivity().findViewById(R.id.getPhone);
        phone.setEnabled(false);
        spinner.setOnItemSelectedListener(this);
        getData();

        from_date = (EditText)getActivity().findViewById(R.id.getReminderFrom);
        to_date = (EditText)getActivity().findViewById(R.id.getReminderTo);

        from_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        from_date.setText(dayOfMonth+ "-" + (monthOfYear + 1) + "-" +  year);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        to_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                nYear = c.get(Calendar.YEAR);
                nMonth = c.get(Calendar.MONTH);
                nDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        to_date.setText(dayOfMonth+ "-" + (monthOfYear + 1) + "-" +  year);
                    }
                }, nYear, nMonth, nDay);
                datePickerDialog.show();
            }
        });

        //sharedpreferences logic
        SharedPref sharedPref = new SharedPref(getActivity());
        uid = sharedPref.getId().toString();
        BackGround b = new BackGround();
        b.execute(uid);
        // sharedpreferences logic


        rem_req = (RadioGroup) view.findViewById(R.id.rem_req);
        getComment = (EditText) view.findViewById(R.id.getComment);
        getsummary = (EditText) view.findViewById(R.id.getsummary);
        getReminderFrom = (EditText) view.findViewById(R.id.getReminderFrom);
        getReminderTo = (EditText) view.findViewById(R.id.getReminderTo);
        scrtocontactname = (Spinner) view.findViewById(R.id.scrtocontactname);
        getCallByRoleID = (Spinner) view.findViewById(R.id.getCallByRoleID);
        backtoeventlist = (Button)view.findViewById(R.id.backtoeventlist);
        addreminder = (Button)view.findViewById(R.id.addreminder);

//        scrtocontactname.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//            {
//                Source_to_contact = parent.getItemAtPosition(position).toString();
//            }
//        });


//
//        getCallByRoleID.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//            {
//                Dept = parent.getItemAtPosition(position).toString();
//            }
//        });
//
//        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//            {
//                GetName = parent.getItemAtPosition(position).toString();
//            }
//        });
//
        rem_req.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.yes:
                        r = "1";
                        break;
                    case R.id.no:
                        r = "0";
                        break;
                }
            }
        });

        addreminder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Source_to_contact = scrtocontactname.getSelectedItem().toString();
                Dept = getCallByRoleID.getSelectedItem().toString();
                GetName = spinner.getSelectedItem().toString();
                Comment = getComment.getText().toString();
                Summery = getsummary.getText().toString();
                Rem_start = getReminderFrom.getText().toString();
                Rem_end = getReminderTo.getText().toString();
                Reminder_req = r;
                Caller_Name = String.valueOf(name.getText());

                BackGround_Worker bw = new BackGround_Worker();
                bw.execute(Reminder_req, Source_to_contact, Comment, Summery, Rem_start, Rem_end, Dept,GetName, UID);
            }
        });

    }//onview created


    class BackGround_Worker extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String reg_req        = params[0];
            String soc            = params[1];
            String comment        = params[2];
            String summery        = params[3];
            String rem_start      = params[4];
            String rem_end        = params[5];
            String dept           = params[6];
            String getname        = params[7];
            String caller_name    = params[8];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/add_event.php");

                String urlParams = "reg_req="+reg_req
                        +"&soc="+soc
                        +"&comment="+comment
                        +"&summery="+ summery
                        +"&rem_start="+rem_start
                        +"&rem_end="+rem_end
                        +"&dept="+ dept
                        +"&getname="+getname
                        +"&caller_name="+caller_name;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();
                return data;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s)
        {

            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                ID = user_data.getString("id");
                msg = user_data.getString("msg");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
                Toast.makeText(getActivity(), "event added successfully", Toast.LENGTH_LONG).show();
                Fragment fragment = new Event_menu();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                //fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                getFragmentManager().popBackStack();
            }
            else
            {
                Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
            }

        }
    }//end of background


    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String id = params[0];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/profile.php");
                String urlParams = "id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }//end of do in background

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                msg = user_data.getString("msg");
                UID = user_data.getString("id");
                NAME = user_data.getString("name");
                PASSWORD = user_data.getString("password");
                EMAIL = user_data.getString("email");
                MOBILE = user_data.getString("mobile");
                FULLNAME = user_data.getString("fullname");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
                name.setText(FULLNAME);
                phone.setText(MOBILE);

            }
            else
            {
                Toast.makeText(getActivity(), "Info loading fails", Toast.LENGTH_LONG).show();
            }
        }//end of onpostexecute
    }//end of background

    private void getData()
    {
        //Creating a string request
        StringRequest stringRequest = new StringRequest(Config.DATA_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                JSONObject j = null;
                try
                {
                    j = new JSONObject(response);
                    result = j.getJSONArray(Config.JSON_ARRAY);
                    getStudents(result);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
        new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void getStudents(JSONArray j)
    {
        for(int i=0;i<j.length();i++)
        {
            try
            {
                JSONObject json = j.getJSONObject(i);
                students.add(json.getString(Config.TAG_USERNAME));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, students));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

}//end of add event class

class Config
{
    public static final String DATA_URL = "http://maierp.in/android_live/demo/user_spinner.php";
    public static final String TAG_USERNAME = "uname";
    public static final String JSON_ARRAY = "result";
}



