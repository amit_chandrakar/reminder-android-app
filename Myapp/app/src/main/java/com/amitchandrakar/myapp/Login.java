package com.amitchandrakar.myapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Login extends Activity
{
    TextView create_account, forgot_password;
    CheckBox show_hide_password;
    EditText name, password;
    String Name, Password;
    Context ctx=this;
    String ID= null, NAME=null, PASSWORD=null, EMAIL=null, MOBILE=null, FULLNAME=null, msg = null;
    Vibrator vibe;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();

        vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);


        name = (EditText) findViewById(R.id.login_emailid);
        password = (EditText) findViewById(R.id.login_password);
        create_account = (TextView)findViewById(R.id.createAccount);
        forgot_password = (TextView)findViewById(R.id.forgot_password);
        show_hide_password = (CheckBox)findViewById(R.id.show_hide_password);

        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Forgot_Password.class);
                startActivity(intent);
            }
        });

        // Set check listener over checkbox for showing and hiding password
        show_hide_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked)
            {
                if (isChecked)
                {
                    show_hide_password.setText(R.string.hide_pwd);
                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());// show password
                }
                else
                {
                    show_hide_password.setText(R.string.show_pwd);
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());// hide password
                }
            }
        });//checkbox

     //sharedpreferences logic
        SharedPref sharedPref = new SharedPref(Login.this);
        if (sharedPref.getId()!="")
        {
            Intent intent = new Intent(Login.this, MainActivity.class);
            intent.putExtra("id", sharedPref.getId());
            startActivity(intent);
            finish();
        }
     // sharedpreferences logic

    }


    public void OnLogin(View v)
    {

        Name = name.getText().toString();
        Password = password.getText().toString();

        if (Name.equals("") || Name.length() == 0)
        {
            vibe.vibrate(100);
            name.setError("This field cannot be empty");
        }
        else if (Password.equals("") || Password.length() == 0)
        {
            vibe.vibrate(100);
            password.setError("This field cannot be empty");
        }
        else
        {

            BackGround b = new BackGround();
            b.execute(Name, Password);
        }
    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String name = params[0];
            String password = params[1];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/login.php");
                String urlParams = "name="+name+"&password="+password;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                msg = user_data.getString("msg");
                ID = user_data.getString("id");
                NAME = user_data.getString("name");
                PASSWORD = user_data.getString("password");
                EMAIL = user_data.getString("email");
                MOBILE = user_data.getString("mobile");
                FULLNAME = user_data.getString("fullname");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
                Toast.makeText(ctx, "Welcome : "+NAME, Toast.LENGTH_LONG).show();

                SharedPref sharedPref = new SharedPref(Login.this);
                sharedPref.setId(ID);

                Intent i = new Intent(Login.this, MainActivity.class);
                i.putExtra("name", NAME);
                i.putExtra("password", PASSWORD);
                i.putExtra("email", EMAIL);
                i.putExtra("mobile", MOBILE);
                i.putExtra("fullname", FULLNAME);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
            else
            {
                vibe.vibrate(100);
                Toast.makeText(ctx, "Incorrect Username or Password..!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Exit Application?");
        alertDialogBuilder
                .setIcon(R.drawable.warning)
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }//end of onbackpress

}//end of main class
