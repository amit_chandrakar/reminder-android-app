package com.amitchandrakar.myapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class Register extends Activity
{
    TextView already_user;
    EditText fullname, username, password, con_password, email, mobile;
    String Fullname, Username, Password, Con_password, Email, Mobile, Dob, Ann_date, Gender, Marital_Status;
    String ID=null;
    Context ctx=this;
    CheckBox terms_conditions;

    ImageView cal_ann_date, cal_dob;
    EditText ET_an_date, ET_dob;
    private int mYear, mMonth, mDay;
    private int nYear, nMonth, nDay;

    String gender , status= "";

    Vibrator vibe;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();

        vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        fullname             = (EditText)findViewById(R.id.fullname);
        username             = (EditText)findViewById(R.id.username);
        password             = (EditText)findViewById(R.id.password);
        con_password         = (EditText)findViewById(R.id.con_password);
        email                = (EditText)findViewById(R.id.email);
        mobile               = (EditText)findViewById(R.id.mobile);
        already_user         = (TextView)findViewById(R.id.already_user);
//        terms_conditions    = (CheckBox)findViewById(R.id.tnc);

        cal_ann_date = (ImageView)findViewById(R.id.btn_ann_date);
        ET_an_date=(EditText)findViewById(R.id.ann_date);

        cal_ann_date.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Register.this, new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        ET_an_date.setText(dayOfMonth+ "-" + (monthOfYear + 1) + "-" +  year);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        cal_dob = (ImageView)findViewById(R.id.btn_dob);
        ET_dob=(EditText)findViewById(R.id.dob);
        cal_dob.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                nYear = c.get(Calendar.YEAR);
                nMonth = c.get(Calendar.MONTH);
                nDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Register.this, new DatePickerDialog.OnDateSetListener()
                {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        ET_dob.setText( year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, nYear, nMonth, nDay);
                datePickerDialog.show();
            }
        });

        already_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, Login.class);
                startActivity(intent);
            }
        });

        RadioGroup get_gender = (RadioGroup) findViewById(R.id.gender);
        get_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.male:
                        gender = "Male";
                        break;
                    case R.id.female:
                        gender = "Female";
                        break;
                }
            }
        });

        RadioGroup get_marital_status = (RadioGroup) findViewById(R.id.marital_status);
        get_marital_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.married:
                        status = "Married";
                        ET_an_date.setVisibility(View.VISIBLE);
                        cal_ann_date.setVisibility(View.VISIBLE);
                        break;
                    case R.id.unmarried:
                        status = "Unmarried";
                        ET_an_date.setVisibility(View.INVISIBLE);
                        cal_ann_date.setVisibility(View.INVISIBLE);
                        ET_dob.setText("");
                        break;
                }
            }
        });


    }//end of oncreate

    public void On_Register(View v)
    {

        Fullname            = fullname.getText().toString();
        Username            = username.getText().toString();
        Password            = password.getText().toString();
        Con_password        = con_password.getText().toString();
        Email               = email.getText().toString();
        Mobile              = mobile.getText().toString();
        Dob                 = ET_dob.getText().toString();
        Ann_date            = ET_an_date.getText().toString();
        Gender              = gender;
        Marital_Status      = status;

        String username_exp = "^[A-Za-z0-9]{4,15}$";
        String email_exp = "^[A-Za-z][A-Za-z0-9]*([._-]?[A-Za-z0-9]+)@[A-Za-z].[A-Za-z]{0,10}?.[A-Za-z]{0,10}$";

        if (Fullname.equals("") || Fullname.length() == 0)
        {
            vibe.vibrate(100);
            fullname.setError("This field cannot be empty");
            Toast.makeText(Register.this, "Invalid Fullname", Toast.LENGTH_LONG).show();
        }
        else if (Username.equals("") || Username.length() == 0)
        {
            vibe.vibrate(100);
            username.setError("This field cannot be empty");
            Toast.makeText(Register.this, "Invalid Username", Toast.LENGTH_LONG).show();
        }
        else if (!Username.matches(username_exp))
        {
            vibe.vibrate(100);
            username.setError("Contain A-Za-z0-9");
            Toast.makeText(Register.this, "Invalid Username", Toast.LENGTH_LONG).show();
        }
        else if (Password.equals("") || Password.length() == 0)
        {
            vibe.vibrate(100);
            password.setError("This field cannot be empty");
            Toast.makeText(Register.this, "Invalid Password", Toast.LENGTH_LONG).show();
        }
        else if ( Password.length() < 5)
        {
            vibe.vibrate(100);
            password.setError("Poor Password");
            Toast.makeText(Register.this, "Invalid Password", Toast.LENGTH_LONG).show();
        }
        else if (!Password.matches(Con_password))
        {
            vibe.vibrate(100);
            con_password.setError("Confirm password not match");
            Toast.makeText(Register.this, "Invalid Password", Toast.LENGTH_LONG).show();
        }
        else if (Email.equals("") || Email.length() == 0)
        {
            vibe.vibrate(100);
            email.setError("This field cannot be empty");
            Toast.makeText(Register.this, "Invalid Email", Toast.LENGTH_LONG).show();
        }
        else if (!Email.matches(email_exp))
        {
            vibe.vibrate(100);
            email.setError("Email Incorrect");
            Toast.makeText(Register.this, "Invalid Email", Toast.LENGTH_LONG).show();
        }
        else if (Mobile.equals("") || Mobile.length() == 0)
        {
            vibe.vibrate(100);
            mobile.setError("This field cannot be empty");
            Toast.makeText(Register.this, "Invalid Mobile Number", Toast.LENGTH_LONG).show();
        }
        else if (Mobile.length()<10 || Mobile.length()>10)
        {
            vibe.vibrate(100);
            mobile.setError("Incorrect Mobile Number");
            Toast.makeText(Register.this, "Invalid Mobile Number", Toast.LENGTH_LONG).show();
        }
        else
        {
            BackGround b = new BackGround();
            b.execute(Fullname, Username, Password, Email, Mobile,Dob, Ann_date, Gender, Marital_Status);
        }

    }//end of on_register

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String FullName        = params[0];
            String User            = params[1];
            String Pass            = params[2];
            String Email           = params[3];
            String Mobile          = params[4];
            String Dob             = params[5];
            String Ann_date        = params[6];
            String Gender          = params[7];
            String M_status        = params[8];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/register.php");

                String urlParams = "full_name="+FullName
                        +"&username="+User
                        +"&password="+Pass
                        +"&email="+ Email
                        +"&mobile="+Mobile
                        +"&dob="+Dob
                        +"&ann_date="+ Ann_date
                        +"&gender="+Gender
                        +"&status="+M_status;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();
                return data;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s)
        {

            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                ID = user_data.getString("id");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            Intent i = new Intent(ctx, Varify_OTP.class);
            i.putExtra("id", ID);
            i.putExtra("err", err);
            startActivity(i);

        }
    }//end of background

}//End of main class
