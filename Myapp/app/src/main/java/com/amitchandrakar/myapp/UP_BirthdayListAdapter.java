package com.amitchandrakar.myapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class UP_BirthdayListAdapter extends BaseAdapter
{
    Context context;
    List<UP_DOB> birthday_list;
    public UP_BirthdayListAdapter(List<UP_DOB> listValue, Context context)
    {
        this.context = context;
        this.birthday_list = listValue;
    }

    @Override
    public int getCount()
    {
        return this.birthday_list.size();
    }

    @Override
    public Object getItem(int position)
    {
        return this.birthday_list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        UP_ViewDOB viewDOB = null;
        if(convertView == null)
        {
            viewDOB = new UP_ViewDOB();
            LayoutInflater layoutInfiater = (LayoutInflater)this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfiater.inflate(R.layout.up_birthday_listview_items, null);
            viewDOB.User_Name = (TextView)convertView.findViewById(R.id.user_name);
            viewDOB.DOB = (TextView)convertView.findViewById(R.id.dob);
            convertView.setTag(viewDOB);
        }
        else
        {
            viewDOB = (UP_ViewDOB) convertView.getTag();
        }
        viewDOB.User_Name.setText(birthday_list.get(position).User_Name_Col);
        viewDOB.DOB.setText(birthday_list.get(position).DOB_Col);
        return convertView;
    }
}//end of list adapter

class UP_ViewDOB
{
    TextView User_Name;
    TextView DOB;
} //end of viewitem



