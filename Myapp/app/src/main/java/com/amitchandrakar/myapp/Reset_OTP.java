package com.amitchandrakar.myapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Reset_OTP extends Activity
{

    EditText otp_box;
    String id;
    TextView back;
    String OTP, MSG, ID;
    Context ctx=this;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset__otp);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();


        otp_box = (EditText)findViewById(R.id.otp);
        back = (TextView)findViewById(R.id.back);
        id = getIntent().getStringExtra("id");

    }//end of Oncreate

    public void On_OTP(View v)
    {

        id = getIntent().getStringExtra("id");
        OTP = otp_box.getText().toString();
        BackGround b = new BackGround();
        b.execute(OTP, id);

    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String otp = params[0];
            String id= params[1];
            String data="";
            int tmp;
            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/reset_otp.php");
                String urlParams = "otp="+otp+"&id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();
                return data;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s)
        {

            String err = null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                MSG = user_data.getString("msg");
                if (MSG.equals("SUCCESS"))
                {
//                    Intent intent = new Intent(ctx, Reset_Password.class);
//                    startActivity(intent);
                    ID = user_data.getString("id");
                    Intent i = new Intent(ctx, Reset_Password.class);
                    i.putExtra("id", ID);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(ctx, "Incorrect OTP..! Please Enter Correct OTP", Toast.LENGTH_LONG).show();
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

        }
    }

}//end of main class
