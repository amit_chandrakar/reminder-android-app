package com.amitchandrakar.myapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Reset_Password extends Activity {

    Context ctx=this;
    EditText password, con_password;
    String Password, Con_password, id;
    String MSG;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset__password);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();


        password = (EditText) findViewById(R.id.pwd);
        con_password = (EditText) findViewById(R.id.c_pwd);
    }
    public void On_Reset(View v)
    {

        id = getIntent().getStringExtra("id");
        Password = password.getText().toString();
        Con_password = con_password.getText().toString();
        if (Password.equals("") || Password.length() == 0)
        {
            password.setError("This field cannot be empty");
        }
        else if ( Password.length() < 5)
        {
            password.setError("Poor Password");
        }
        else if (!Password.matches(Con_password))
        {
            con_password.setError("Confirm password not match");
        }
        else
        {
            BackGround b = new BackGround();
            b.execute(Password, id);
        }
    }

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String password = params[0];
            String id = params[1];
            String data="";
            int tmp;
            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/reset_password.php");
                String urlParams = "password="+password+"&id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();
                return data;

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s)
        {

            String err = null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                MSG = user_data.getString("msg");
                if (MSG.equals("SUCCESS"))
                {
                    Toast.makeText(ctx, "Password Reset Successfully", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ctx, Login.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(ctx, "Error...! Try Again Later", Toast.LENGTH_LONG).show();
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

        }
    }



}//end of main class
