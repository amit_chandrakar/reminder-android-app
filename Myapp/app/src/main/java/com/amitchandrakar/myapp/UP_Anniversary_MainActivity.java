package com.amitchandrakar.myapp;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UP_Anniversary_MainActivity extends Activity
{
//    ListView SubjectFullFormListView;
//    ProgressBar progressBar1;
//    String HttpURL1 = "http://maierp.in/android_live/demo/fetch_user_ann_date.php";
//    UP_ListAdapter adapter1 ;
//    List<UP_Subject> SubjectFullFormList;
//
//    ListView DOB_ListView;
//    ProgressBar progressBar2;
//    String HttpURL2 = "http://maierp.in/android_live/demo/fetch_user_list.php";
//    UP_BirthdayListAdapter adapter2 ;
//    List<UP_DOB> DOB_List;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.up_anniversary_activity_main);
//
//        SubjectFullFormListView = (ListView) findViewById(R.id.ann_list);
//        progressBar1 = (ProgressBar) findViewById(R.id.ProgressBar1);
//        new ParseJSonDataClass1(this).execute();
//
//        DOB_ListView = (ListView) findViewById(R.id.bir_list);
//        progressBar2 = (ProgressBar) findViewById(R.id.ProgressBar2);
//        new ParseJSonDataClass2(this).execute();
//    }
//
//    private class ParseJSonDataClass1 extends AsyncTask<Void, Void, Void>
//    {
//        public Context context;
//        String FinalJSonResult;
//        public ParseJSonDataClass1(Context context)
//        {
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected Void doInBackground(Void... arg0)
//        {
//            UP_HttpServiceClass httpServiceClass = new UP_HttpServiceClass(HttpURL1);
//            try
//            {
//                httpServiceClass.ExecutePostRequest();
//                if (httpServiceClass.getResponseCode() == 200)
//                {
//                    FinalJSonResult = httpServiceClass.getResponse();
//                    if (FinalJSonResult != null) {
//                        JSONArray jsonArray = null;
//                        try
//                        {
//                            jsonArray = new JSONArray(FinalJSonResult);
//                            JSONObject jsonObject;
//                            UP_Subject subject;
//                            SubjectFullFormList = new ArrayList<UP_Subject>();
//                            for (int i = 0; i < jsonArray.length(); i++)
//                            {
//                                subject = new UP_Subject();
//                                jsonObject = jsonArray.getJSONObject(i);
//                                subject.Subject_Name = jsonObject.getString("uname");
//                                subject.Subject_Full_Form = jsonObject.getString("anniversary");
//                                SubjectFullFormList.add(subject);
//                            }
//                        }
//                        catch (JSONException e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//                else
//                {
//                    Toast.makeText(context, httpServiceClass.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result)
//
//        {
//            progressBar1.setVisibility(View.GONE);
//            SubjectFullFormListView.setVisibility(View.VISIBLE);
//            adapter1 = new UP_ListAdapter(SubjectFullFormList, context);
//            SubjectFullFormListView.setAdapter(adapter1);
//        }
//    }
//
//    private class ParseJSonDataClass2 extends AsyncTask<Void, Void, Void>
//    {
//        public Context context;
//        String FinalJSonResult;
//        public ParseJSonDataClass2(Context context)
//        {
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected Void doInBackground(Void... arg0)
//        {
//            UP_Birthday_HttpServiceClass birthday_httpServiceClass = new UP_Birthday_HttpServiceClass(HttpURL2);
//            try
//            {
//                birthday_httpServiceClass.ExecutePostRequest();
//                if (birthday_httpServiceClass.getResponseCode() == 200)
//                {
//                    FinalJSonResult = birthday_httpServiceClass.getResponse();
//                    if (FinalJSonResult != null)
//                    {
//                        JSONArray jsonArray = null;
//                        try
//                        {
//                            jsonArray = new JSONArray(FinalJSonResult);
//                            JSONObject jsonObject;
//                            UP_DOB dob;
//                            DOB_List = new ArrayList<UP_DOB>();
//                            for (int i = 0; i < jsonArray.length(); i++)
//                            {
//                                dob = new UP_DOB();
//                                jsonObject = jsonArray.getJSONObject(i);
//                                dob.User_Name_Col = jsonObject.getString("uname");
//                                dob.DOB_Col = jsonObject.getString("udob");
//                                DOB_List.add(dob);
//                            }
//                        }
//                        catch (JSONException e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//                else
//                {
//                    Toast.makeText(context, birthday_httpServiceClass.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            progressBar2.setVisibility(View.GONE);
//            DOB_ListView.setVisibility(View.VISIBLE);
//            adapter2 = new UP_BirthdayListAdapter(DOB_List, context);
//            DOB_ListView.setAdapter(adapter2);
//        }
//    }

}

//class UP_Subject
//{
//    public String Subject_Name;
//    public String Subject_Full_Form;
//}
//
//class UP_DOB
//{
//    public String User_Name_Col;
//    public String DOB_Col;
//}
