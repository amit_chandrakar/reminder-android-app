package com.amitchandrakar.myapp;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref
{
    private String id;
    SharedPreferences sharedPreferences;


    public String getId()
    {
        id = sharedPreferences.getString("userdata","");
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
        sharedPreferences.edit().putString("userdata", id).commit();
    }

    public SharedPref(Context context)
    {
        sharedPreferences = context.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
    }

    public void removeuser()
    {
        sharedPreferences.edit().clear().commit();
    }
}
