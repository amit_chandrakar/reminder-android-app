package com.amitchandrakar.myapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Profile extends Activity
{
    String ID= null, NAME=null, PASSWORD=null, EMAIL=null, MOBILE=null, FULLNAME=null, msg = null;
    TextView nameTV, emailTV, passwordTV, mobileTV, fullnameTV;
    String uid;
    Context ctx=this;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);


        //sharedpreferences logic
        SharedPref sharedPref = new SharedPref(Profile.this);
        uid = sharedPref.getId().toString();
        BackGround b = new BackGround();
        b.execute(uid);
        // sharedpreferences logic

        nameTV = (TextView) findViewById(R.id.user_profile_name);
        emailTV = (TextView) findViewById(R.id.user_email);
        passwordTV = (TextView) findViewById(R.id.password);
        mobileTV = (TextView) findViewById(R.id.mobile);
        fullnameTV = (TextView) findViewById(R.id.fullname);
        imageView = (ImageView) findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
//                Intent intent = new Intent(Profile.this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                onBackPressed();
            }
        });


        final ImageView drop_down_option_menu = (ImageView) findViewById(R.id.drop_down_option_menu);
        drop_down_option_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(Profile.this, drop_down_option_menu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.profile, popup.getMenu());
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        int id = item.getItemId();
                        if (id == R.id.edit_profile)
                        {
//                            Toast.makeText(Profile.this, "option", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Profile.this, Edit_profile.class);
                            startActivity(intent);
                        }

                        return true;
                    }
                });
                popup.show(); //showing popup menu
            }
        });


    }//end of oncreate

    class BackGround extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String id = params[0];
            String data="";
            int tmp;

            try
            {
                URL url = new URL("http://maierp.in/android_live/demo/profile.php");
                String urlParams = "id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1)
                {
                    data+= (char)tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }//end of do in background

        @Override
        protected void onPostExecute(String s)
        {
            String err=null;
            try
            {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                msg = user_data.getString("msg");
                NAME = user_data.getString("name");
                PASSWORD = user_data.getString("password");
                EMAIL = user_data.getString("email");
                MOBILE = user_data.getString("mobile");
                FULLNAME = user_data.getString("fullname");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            if (msg.equals("SUCCESS"))
            {
               nameTV.setText(FULLNAME);
               passwordTV.setText(PASSWORD);
              emailTV.setText(EMAIL);
              mobileTV.setText(MOBILE);
              fullnameTV.setText(NAME);
            }
            else
            {
                Toast.makeText(ctx, "Info loading fails", Toast.LENGTH_LONG).show();
            }
        }//end of onpostexecute
    }//end of background

    @Override
    public void onBackPressed()
    {
            super.onBackPressed();
    }

}//end of main class
