-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 29, 2018 at 05:11 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `VNRSEEDS`
--

-- --------------------------------------------------------

--
-- Table structure for table `alert`
--

CREATE TABLE `alert` (
  `id` int(12) NOT NULL,
  `notificationtitle` varchar(100) NOT NULL,
  `notificationtext` varchar(200) NOT NULL,
  `n_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alert`
--

INSERT INTO `alert` (`id`, `notificationtitle`, `notificationtext`, `n_date`) VALUES
(1, 'Holiday', 'today is holiday.', '2018-08-16'),
(4, 'DEAD...!', 'Atal Bihari ji dead at the age of 90.', '2018-08-20'),
(3, 'alert..!', 'Sunday is not day off.', '2018-08-20');

-- --------------------------------------------------------

--
-- Table structure for table `demo_table`
--

CREATE TABLE `demo_table` (
  `id` int(11) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `upwd` varchar(100) NOT NULL,
  `uemail` varchar(200) NOT NULL,
  `umobile` varchar(100) NOT NULL,
  `ufullname` varchar(200) NOT NULL,
  `udob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `material_status` varchar(20) NOT NULL,
  `anniversary` date NOT NULL,
  `OTP` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OTP_ExpiredTime` time NOT NULL,
  `OTP_verification` varchar(50) NOT NULL,
  `usertype` varchar(10) NOT NULL,
  `userstatus` varchar(20) NOT NULL,
  `pwchange_date` date NOT NULL,
  `pwchange_time` time NOT NULL,
  `profilepic` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_table`
--

INSERT INTO `demo_table` (`id`, `uname`, `upwd`, `uemail`, `umobile`, `ufullname`, `udob`, `gender`, `material_status`, `anniversary`, `OTP`, `created_at`, `OTP_ExpiredTime`, `OTP_verification`, `usertype`, `userstatus`, `pwchange_date`, `pwchange_time`, `profilepic`) VALUES
(1, 'amit', 'amit', 'amitchandrakar028@gmail.com', '9826485847', 'Amit  Chandrakar', '1970-08-20', 'Male', 'Unmarried', '1970-01-01', '9470', '2018-08-10 10:21:20', '00:00:00', 'yes', '', '', '0000-00-00', '00:00:00', ''),
(2, 'vivuavx', 'llllll', 'uccjdejejh@gmail.com', '9826485847', 'Cu08yc', '1970-08-20', 'Male', 'Unmarried', '2018-08-03', '5696', '2018-08-08 07:06:07', '00:00:00', 'yes', '', '', '0000-00-00', '00:00:00', ''),
(3, 'fghjc', 'dfgnc', 'gfdn@dfh.df', '9966996699', 'fbdfbc', '2000-08-15', 'Male', 'Unmarried', '1970-01-01', '9755', '2018-08-08 13:31:43', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(4, 'cdsac', 'csac', 'cas', 'csac', 'csac', '2018-08-09', 'cas', 'csa', '2018-08-25', 'csa', '2018-08-08 13:32:10', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(5, 'ger', 'grg', 'gre', 'csac', 'csac', '2018-08-09', 'cas', 'gre', '2018-08-05', 'csa', '2018-08-08 13:32:32', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(6, 'dsf', 'gdsg', 'sdgds', 'ggw', 'wge', '2018-07-09', 'fgnb', 'csa', '2018-09-25', 'csa', '2018-08-08 13:33:04', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(7, 'fdb', 'dfb', 'sdbf', 'bdfs', 'csac', '2018-08-09', 'cas', 'csa', '2018-08-25', 'csa', '2018-08-08 13:33:43', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(8, 'eghtrhh', 'fdehb', 'cas', 'csac', 'csac', '2018-08-09', 'cas', 'csa', '2017-08-25', 'csa', '2018-08-08 13:33:43', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(9, 'qqq', 'qqq', 'qq', 'ukyujk', 'afgag', '2010-08-09', 'cas', 'csa', '2018-12-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(10, 'eee', 'qqq', 'qq', 'ukyujk', 'afgag', '2000-08-09', 'cas', 'csa', '2018-10-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(11, 'yyy', 'yy', 'y', 'u', 'u', '2010-08-23', 'cas', 'csa', '2018-06-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(12, 'qqq', 'qqq', 'qq', 'ukyujk', 'afgag', '2010-08-09', 'cas', 'csa', '2018-12-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(13, 'eee', 'qqq', 'qq', 'ukyujk', 'afgag', '2000-08-09', 'cas', 'csa', '2018-10-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', ''),
(14, 'yyy', 'yy', 'y', 'u', 'u', '2010-08-23', 'cas', 'csa', '2018-06-25', 'csa', '2018-08-08 13:35:30', '00:00:00', '', '', '', '0000-00-00', '00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(12) NOT NULL,
  `rem_req` int(1) NOT NULL,
  `source_to_con` varchar(12) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `start_date_time` varchar(50) NOT NULL,
  `end_date_time` varchar(50) NOT NULL,
  `par_dept` varchar(30) NOT NULL,
  `par_name` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `modified_by` int(12) NOT NULL,
  `modified_at` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `rem_req`, `source_to_con`, `subject`, `description`, `start_date_time`, `end_date_time`, `par_dept`, `par_name`, `created_by`, `created_at`, `modified_by`, `modified_at`) VALUES
(1, 1, 'Meeting', 'Saturday leaves.', 'from 1st September, every Saturdays is leave day.', '2018-08-28', '2018-08-28', 'IT', 'amit', '1', '', 0, ''),
(2, 0, 'Telephonic', 'laptop issue.', 'wifi not working.', '2018-08-31', '2018-08-31', 'Account', 'amit', '1', '', 0, ''),
(3, 0, 'Telephonic', 'laptop issue', 'wifi not working.', '2018-08-31', '2018-08-31', 'Finance', 'amit', '1', '', 0, ''),
(4, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, ''),
(5, 1, 'Telephonic', 'laptop issue.', 'wifi not working', '2018-08-31', '2018-08-31', 'Director', 'amit', '1', '', 0, ''),
(6, 0, 'Telephonic', 'usrufd', 'iditd', '2018-08-31', '2018-08-31', 'Director', 'amit', '1', '', 0, ''),
(7, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, ''),
(8, 0, '', '', '', '1970-01-01', '1970-01-01', '', '', '', '', 0, ''),
(9, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, ''),
(10, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, ''),
(11, 1, 'Conference', 'leave', 'today is leave', '2018-08-27', '2018-08-31', 'Marketing', 'amit', '1', '', 0, ''),
(12, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, ''),
(13, 0, 'Telephonic', '', '', '1970-01-01', '1970-01-01', 'Director', 'amit', '1', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `event_user_detail`
--

CREATE TABLE `event_user_detail` (
  `event_id` int(12) NOT NULL,
  `participants` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_table`
--
ALTER TABLE `demo_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alert`
--
ALTER TABLE `alert`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `demo_table`
--
ALTER TABLE `demo_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
